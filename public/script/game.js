let signXO = 'X';
let num = 0;

function clickBox (box) {
  if (box.innerText === '') {
    num++;
    box.innerText = signXO;
    switchTurn();
    document.getElementById('top-text1').innerText = 'Next round: ' + signXO;
    checkWin();
  }
  if (num > 8) {
    document.getElementById('top-text2').innerText = 'Game ends as a tie!';
  }
}

function switchTurn () {
  signXO === 'X' ? signXO = 'O' : signXO = 'X';
}

function checkWin () {
  const allSigns = [];
  allSigns.push(document.getElementById('square1').innerText);
  allSigns.push(document.getElementById('square2').innerText);
  allSigns.push(document.getElementById('square3').innerText);
  allSigns.push(document.getElementById('square4').innerText);
  allSigns.push(document.getElementById('square5').innerText);
  allSigns.push(document.getElementById('square6').innerText);
  allSigns.push(document.getElementById('square7').innerText);
  allSigns.push(document.getElementById('square8').innerText);
  allSigns.push(document.getElementById('square9').innerText);

  if ((allSigns[0] === allSigns[1] && allSigns[1] === allSigns[2] && allSigns[0] !== '') ||
      (allSigns[0] === allSigns[3] && allSigns[3] === allSigns[6] && allSigns[0] !== '') ||
      (allSigns[0] === allSigns[4] && allSigns[4] === allSigns[8] && allSigns[0] !== '')) {
    document.getElementById('top-text2').innerText = 'Congratulations, ' + allSigns[0] + ' wins!';
  } else if (allSigns[3] === allSigns[4] && allSigns[4] === allSigns[5] && allSigns[3] !== '') {
    document.getElementById('top-text2').innerText = 'Congratulations, ' + allSigns[3] + ' wins!';
  } else if (allSigns[6] === allSigns[7] && allSigns[7] === allSigns[8] && allSigns[6] !== '') {
    document.getElementById('top-text2').innerText = 'Congratulations, ' + allSigns[6] + ' wins!';
  } else if (allSigns[1] === allSigns[4] && allSigns[4] === allSigns[7] && allSigns[1] !== '') {
    document.getElementById('top-text2').innerText = 'Congratulations, ' + allSigns[1] + ' wins!';
  } else if ((allSigns[2] === allSigns[5] && allSigns[5] === allSigns[8] && allSigns[2] !== '') ||
             (allSigns[2] === allSigns[4] && allSigns[4] === allSigns[6] && allSigns[2] !== '')) {
    document.getElementById('top-text2').innerText = 'Congratulations, ' + allSigns[2] + ' wins!';
  }
}

function resetGame () {
  signXO = 'X';
  document.getElementById('top-text1').innerText = 'Next round: ' + signXO;
  document.getElementById('top-text2').innerText = 'Playing ^_^';
  num = 0;
  document.getElementById('square1').innerText = '';
  document.getElementById('square2').innerText = '';
  document.getElementById('square3').innerText = '';
  document.getElementById('square4').innerText = '';
  document.getElementById('square5').innerText = '';
  document.getElementById('square6').innerText = '';
  document.getElementById('square7').innerText = '';
  document.getElementById('square8').innerText = '';
  document.getElementById('square9').innerText = '';
}